﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Forms;


namespace TeamWorkPro
{
    public partial class Screenshot : Form
    {
        public Screenshot()
        {
            InitializeComponent();
        }

        private void Screenshot_Load(object sender, EventArgs e)
        {
            Bitmap memoryImage;
            memoryImage = new Bitmap(1000, 900);
            Size s = new Size(memoryImage.Width, memoryImage.Height);

            Graphics memoryGraphics = Graphics.FromImage(memoryImage);

            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);

            //That's it! Save the image in the directory and this will work like charm.  
            string fileName = string.Format(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                      @"\Screenshot" + "_" +
                      DateTime.Now.ToString("(dd_MMMM_hh_mm_ss_tt)") + ".png");

            // save it  
           // memoryImage.Save(fileName);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string filePath = string.Format(Application.StartupPath + "\\ScreenCapture\\" +
                      @"\Screenshot" + "_" +DateTime.Now.ToString("dd_MMMM_hh_mm_ss_tt") + ".png");
            var ScreenRectangle = GetScreen();
            Bitmap memoryImage;
            var widht = ScreenRectangle.Width;
            var Height = ScreenRectangle.Height;

            memoryImage = new Bitmap(widht, Height);
            Size s = new Size(widht,Height);

            Graphics memoryGraphics = Graphics.FromImage(memoryImage);

            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
            string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            

            //That's it! Save the image in the directory and this will work like charm.  
         string fileName = string.Format(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) +
                      @"\Screenshot" + "_" +
                      DateTime.Now.ToString("(dd_MMMM_hh_mm_ss_tt)") + ".png");

            // save it  
            if(Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            
            memoryImage.Save(filePath);
        }

        public Rectangle GetScreen()
        {
            return Screen.FromControl(this).Bounds;
        }
    }
}
