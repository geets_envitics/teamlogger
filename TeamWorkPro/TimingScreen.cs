﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TeamWorkPro.BusinessLayer;
using TeamWorkPro.DataLayer;
using Win32_API;

namespace TeamWorkPro
{
    public partial class TimingScreen : Form
    {
        TimingLogBaL objLoginBAL = new TimingLogBaL();
        ServiceLogBAL objSync = new ServiceLogBAL();
        ConnectivityBAL ObjectConnectivity = new ConnectivityBAL();
        private LogWriter log = new LogWriter();
        int Hours = 0;
        int Minutes = 0;
        int ImageTimeCount = 0;
        private bool dragging = false;
        private Point startPoint = new Point(0, 0);

        public TimingScreen()
        {
            InitializeComponent();
            HoursTimer = new Timer();
            HoursTimer.Tick += HoursTimer_Tick;
            HoursTimer.Interval = 1000 * TeamWorkPro.Properties.Settings.Default.TimerInterval;
            HoursTimer.Start();
            GlobalVariable.PreviousDateTime = DateTime.Now;
            if (!string.IsNullOrEmpty(GlobalVariable.ProjectName))
            {
                lblProectName.Text = GlobalVariable.ProjectName.Trim();
            }
            if (!string.IsNullOrEmpty(GlobalVariable.TaskName))
            {
                lblTaskName.Text = GlobalVariable.TaskName.Trim();
            }
        }

        private void HoursTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                ImageTimeCount = ImageTimeCount + 1;
                if (ImageTimeCount == Properties.Settings.Default.ScreenCaptureInterval)
                {
                    string filePath = string.Format(Application.StartupPath + "\\ScreenCapture\\");
                    log = new LogWriter(filePath);
                    var ScreenRectangle = GetScreen();
                    Bitmap memoryImage;
                    var widht = ScreenRectangle.Width;
                    var Height = ScreenRectangle.Height;
                    memoryImage = new Bitmap(widht, Height);
                    Size s = new Size(widht, Height);
                    Graphics memoryGraphics = Graphics.FromImage(memoryImage);
                    memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                    if (!Directory.Exists(filePath))
                    {
                        //Directory.CreateDirectory(filePath);
                        DirectoryInfo di = Directory.CreateDirectory(filePath);
                        di.Attributes = FileAttributes.Directory | FileAttributes.Hidden;
                    }
                    var ImageName= GlobalVariable.EmployeeId + "_" + DateTime.Now.ToString("dd_MMMM_hh_mm_ss_tt") + ".png";
                    filePath = filePath + ImageName;
                    ImageTimeCount = 1;
                    memoryImage.Save(filePath);
                    string Base64Image = string.Empty;
                    objLoginBAL.SaveScreenShot(ImageName, filePath, GlobalVariable.EmployeeId, Base64Image);
                    bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
                    if(IsConnectivity)
                    {
                        objSync.SyncScreenShot();
                    }

                    
                }
                TimeSpan span = DateTime.Now.Subtract(GlobalVariable.PreviousDateTime);
                var mninute = span.TotalMinutes;
                if (mninute < 3 && mninute > 0)
                {
                    Minutes = Minutes + 1;
                    if (Minutes == 60)
                    {
                        Hours = Hours + 1;
                        Minutes = 0;
                    }
                    string DisplayHours = Hours.ToString();
                    if (Hours < 10)
                    {
                        DisplayHours = DisplayHours.PadLeft(2, '0');
                    }
                    string DisplayMinutes = Minutes.ToString();
                    if (Hours < 10)
                    {
                        DisplayMinutes = DisplayMinutes.PadLeft(2, '0');
                    }
                    lblCurrentTiming.Text = DisplayHours + " hours " + DisplayMinutes + " minutes ";
                    TimeInTimeOutModel objLoginModel = new TimeInTimeOutModel();
                    TimingLogBaL objTimingBal = new TimingLogBaL();
                    var msg = Win32.GetIdleTime();
                    var Minute = TeamWorkPro.Properties.Settings.Default.IdealTime; //300000 milliseconds (300*1000)
                    objLoginModel.Employee_id = GlobalVariable.EmployeeId;
                    objTimingBal.SaveTimeOutLog(objLoginModel);
                    GlobalVariable.PreviousDateTime = DateTime.Now;
                    if (msg > Minute)
                    {
                        TimeInTimeOutModel objTimeInOutModel = new TimeInTimeOutModel();
                        HoursTimer.Stop();
                        MessageBox.Show("Timer has stopped because you have not interacted with the computer for 5 minutes.", "TeamWork");
                        objTimingBal.IdealSaveTimeOutLog(objLoginModel);
                        objTimingBal.SaveIdealTimeLog(objLoginModel);
                        HomeScreen myForm = new HomeScreen();
                        this.Hide();
                        myForm.ShowDialog();
                        this.Close();
                    }
                }
                else
                {
                    HomeScreen myForm = new HomeScreen();
                    HoursTimer.Stop();
                    this.Hide();
                    myForm.ShowDialog();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                new LogWriter(error);
            }
        }

        public Rectangle GetScreen()
        {
            return Screen.FromControl(this).Bounds;
        }

        private void Login_Click(object sender, EventArgs e)
        {
            TimeInTimeOutModel objTimeInOutModel = new TimeInTimeOutModel();
            objTimeInOutModel.Employee_id = GlobalVariable.EmployeeId;
            objTimeInOutModel.TimeType = 2;
            objLoginBAL.SaveTimeOutLog(objTimeInOutModel);
            HoursTimer.Stop();
            HomeScreen myForm = new HomeScreen();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            TimeInTimeOutModel objTimeInOutModel = new TimeInTimeOutModel();
            objTimeInOutModel.Employee_id = GlobalVariable.EmployeeId;
            objTimeInOutModel.TimeType = 2;
            objLoginBAL.SaveTimeOutLog(objTimeInOutModel);
            HoursTimer.Stop();
            Environment.Exit(0);
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void TimingScreen_Load(object sender, EventArgs e)
        {
            //HoursTimer.Start();
        }

        private void btnClose_MouseHover(object sender, EventArgs e)
        {
            this.btnClose.BackColor = Color.Red;

        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            this.btnClose.BackColor = Color.Transparent;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.startPoint.X, p.Y - this.startPoint.Y);

            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            startPoint = new Point(e.X, e.Y);

        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }
    }
}
