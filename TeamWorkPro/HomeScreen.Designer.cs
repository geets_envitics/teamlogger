﻿namespace TeamWorkPro
{
    partial class HomeScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeScreen));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblSynced = new System.Windows.Forms.Label();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.btnCancle = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cdProject = new System.Windows.Forms.ComboBox();
            this.btnStartTimer = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblTotalTimeCount = new System.Windows.Forms.Label();
            this.pnlProjectList = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblUserName = new System.Windows.Forms.Label();
            this.btnLogout = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.testbtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cbTaskList = new System.Windows.Forms.ComboBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.TimerSync = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblSynced);
            this.panel1.Controls.Add(this.btnMinimize);
            this.panel1.Controls.Add(this.btnCancle);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(447, 29);
            this.panel1.TabIndex = 1;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // lblSynced
            // 
            this.lblSynced.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSynced.AutoSize = true;
            this.lblSynced.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSynced.ForeColor = System.Drawing.Color.Red;
            this.lblSynced.Location = new System.Drawing.Point(6, 6);
            this.lblSynced.Name = "lblSynced";
            this.lblSynced.Size = new System.Drawing.Size(54, 16);
            this.lblSynced.TabIndex = 4;
            this.lblSynced.Text = "Synced";
            // 
            // btnMinimize
            // 
            this.btnMinimize.BackColor = System.Drawing.Color.Transparent;
            this.btnMinimize.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.Color.White;
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.Location = new System.Drawing.Point(389, 0);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(29, 29);
            this.btnMinimize.TabIndex = 0;
            this.btnMinimize.UseVisualStyleBackColor = false;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnCancle
            // 
            this.btnCancle.BackColor = System.Drawing.Color.Transparent;
            this.btnCancle.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCancle.FlatAppearance.BorderSize = 0;
            this.btnCancle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancle.ForeColor = System.Drawing.Color.White;
            this.btnCancle.Image = ((System.Drawing.Image)(resources.GetObject("btnCancle.Image")));
            this.btnCancle.Location = new System.Drawing.Point(418, 0);
            this.btnCancle.Name = "btnCancle";
            this.btnCancle.Size = new System.Drawing.Size(29, 29);
            this.btnCancle.TabIndex = 1;
            this.btnCancle.UseVisualStyleBackColor = false;
            this.btnCancle.Click += new System.EventHandler(this.btnCancle_Click);
            this.btnCancle.MouseLeave += new System.EventHandler(this.btnCancle_MouseLeave);
            this.btnCancle.MouseHover += new System.EventHandler(this.btnCancle_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel2.Controls.Add(this.cdProject);
            this.panel2.Location = new System.Drawing.Point(2, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(448, 47);
            this.panel2.TabIndex = 2;
            // 
            // cdProject
            // 
            this.cdProject.DropDownHeight = 110;
            this.cdProject.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cdProject.FormattingEnabled = true;
            this.cdProject.IntegralHeight = false;
            this.cdProject.ItemHeight = 16;
            this.cdProject.Location = new System.Drawing.Point(69, 13);
            this.cdProject.Name = "cdProject";
            this.cdProject.Size = new System.Drawing.Size(292, 24);
            this.cdProject.TabIndex = 4;
            this.cdProject.Text = "Select Project";
            this.cdProject.SelectedIndexChanged += new System.EventHandler(this.cdProject_SelectedIndexChanged);
            // 
            // btnStartTimer
            // 
            this.btnStartTimer.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnStartTimer.FlatAppearance.BorderSize = 0;
            this.btnStartTimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartTimer.ForeColor = System.Drawing.Color.White;
            this.btnStartTimer.Location = new System.Drawing.Point(2, 132);
            this.btnStartTimer.Name = "btnStartTimer";
            this.btnStartTimer.Size = new System.Drawing.Size(448, 35);
            this.btnStartTimer.TabIndex = 4;
            this.btnStartTimer.Text = "Start Timer";
            this.btnStartTimer.UseVisualStyleBackColor = false;
            this.btnStartTimer.Click += new System.EventHandler(this.btnStartTimer_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.lblTotalTimeCount);
            this.panel5.Location = new System.Drawing.Point(2, 170);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(448, 27);
            this.panel5.TabIndex = 8;
            // 
            // lblTotalTimeCount
            // 
            this.lblTotalTimeCount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTotalTimeCount.AutoSize = true;
            this.lblTotalTimeCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalTimeCount.Location = new System.Drawing.Point(4, 6);
            this.lblTotalTimeCount.Name = "lblTotalTimeCount";
            this.lblTotalTimeCount.Size = new System.Drawing.Size(189, 16);
            this.lblTotalTimeCount.TabIndex = 5;
            this.lblTotalTimeCount.Text = "Time Logged Today : 00:00:00";
            // 
            // pnlProjectList
            // 
            this.pnlProjectList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlProjectList.AutoScroll = true;
            this.pnlProjectList.BackColor = System.Drawing.Color.White;
            this.pnlProjectList.Location = new System.Drawing.Point(2, 228);
            this.pnlProjectList.Name = "pnlProjectList";
            this.pnlProjectList.Size = new System.Drawing.Size(448, 335);
            this.pnlProjectList.TabIndex = 9;
            this.pnlProjectList.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlProjectList_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "Last Task";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.SkyBlue;
            this.panel7.Controls.Add(this.lblUserName);
            this.panel7.Controls.Add(this.btnLogout);
            this.panel7.Location = new System.Drawing.Point(1, 564);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(448, 25);
            this.panel7.TabIndex = 10;
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(11, 5);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(45, 16);
            this.lblUserName.TabIndex = 6;
            this.lblUserName.Text = "label2";
            // 
            // btnLogout
            // 
            this.btnLogout.BackColor = System.Drawing.Color.Transparent;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogout.ForeColor = System.Drawing.Color.Red;
            this.btnLogout.Location = new System.Drawing.Point(373, 0);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(75, 25);
            this.btnLogout.TabIndex = 2;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = false;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.Controls.Add(this.testbtn);
            this.panel8.Controls.Add(this.button1);
            this.panel8.Controls.Add(this.label1);
            this.panel8.Location = new System.Drawing.Point(2, 199);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(448, 27);
            this.panel8.TabIndex = 6;
            // 
            // testbtn
            // 
            this.testbtn.BackColor = System.Drawing.Color.Transparent;
            this.testbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.testbtn.Image = ((System.Drawing.Image)(resources.GetObject("testbtn.Image")));
            this.testbtn.Location = new System.Drawing.Point(423, 2);
            this.testbtn.Name = "testbtn";
            this.testbtn.Size = new System.Drawing.Size(22, 22);
            this.testbtn.TabIndex = 0;
            this.testbtn.UseVisualStyleBackColor = false;
            this.testbtn.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(398, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(22, 22);
            this.button1.TabIndex = 6;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            // 
            // cbTaskList
            // 
            this.cbTaskList.DropDownHeight = 110;
            this.cbTaskList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cbTaskList.FormattingEnabled = true;
            this.cbTaskList.IntegralHeight = false;
            this.cbTaskList.ItemHeight = 16;
            this.cbTaskList.Location = new System.Drawing.Point(15, 13);
            this.cbTaskList.Name = "cbTaskList";
            this.cbTaskList.Size = new System.Drawing.Size(418, 24);
            this.cbTaskList.TabIndex = 3;
            this.cbTaskList.Text = "Select Task";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.cbTaskList);
            this.panel3.Location = new System.Drawing.Point(3, 81);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(447, 48);
            this.panel3.TabIndex = 0;
            // 
            // HomeScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(452, 592);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.btnStartTimer);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.pnlProjectList);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HomeScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomeScreen";
            this.Load += new System.EventHandler(this.HomeScreen_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblSynced;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.Button btnCancle;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnStartTimer;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label lblTotalTimeCount;
        private System.Windows.Forms.Panel pnlProjectList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Button testbtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cbTaskList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cdProject;
        private System.Windows.Forms.Timer TimerSync;
        private System.Windows.Forms.Label lblUserName;
    }
}