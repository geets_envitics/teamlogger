﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkPro.DataLayer
{
    public class TimeInTimeOutModel
    {
        public string Employee_id { get; set; }
        public string Date { get; set; }
        public string time_In { get; set; }
        public string time_out { get; set; }
        public int TimeType { get; set; }

    }
}
