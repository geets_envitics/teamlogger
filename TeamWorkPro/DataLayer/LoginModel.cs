﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkPro.DataLayer
{
    public class LoginModel
    {
        public string Employee_id { get; set; }
        public string Employee_Name { get; set; }
        public string Token { get; set; }
        public string Date { get; set; }
        public bool IsRemembe { get; set; }

    }
}
