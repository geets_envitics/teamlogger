﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkPro.BusinessLayer;

namespace TeamWorkPro.DataLayer
{
   public  class TimingSync
    {
        public string Employee_Id { get; set; }
        public string Employee_Name { get; set; }
        public string Date { get; set; }
        public string time_In { get; set; }
        public string time_out { get; set; }
        public Project Project { get; set; }
        public Tasks Task { get; set; }

    }
}
