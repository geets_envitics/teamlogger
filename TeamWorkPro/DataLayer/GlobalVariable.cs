﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkPro.DataLayer
{
    #region Globally used variable
    public static class GlobalVariable
    {
        public static  string ProjectName { get; set; }
        public static string TaskName { get; set; }
        public static string EmployeeId { get; set; }
        public static string ProjectId { get; set; }
        public static string EmployeeName { get; set; }
        public static DateTime CurrentDate { get; set; }
        public static string Token { get; set; }
        public static DateTime PreviousDateTime { get; set; }
        
    }
    #endregion
}
