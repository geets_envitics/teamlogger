﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkPro.DataLayer
{
    public class ProjectListModel
    {
        public string ProjectId { get; set; }
        public string TaskName { get; set; }
        public string pname { get; set; }
        public string TaskId { get; set; }
    }
}
