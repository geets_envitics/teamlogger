﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamWorkPro.BusinessLayer
{
    public class SyncScreenShotModel
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Employee_Id { get; set; }
        public string ImageBase64 { get; set; }

    }
}
