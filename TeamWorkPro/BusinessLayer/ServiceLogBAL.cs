﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using TeamWorkPro.DataLayer;
using Newtonsoft.Json;
using System.Drawing;
using System.Threading.Tasks;

namespace TeamWorkPro.BusinessLayer
{
    public class ServiceLogBAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        private LogWriter log = new LogWriter();
        public ServiceLogBAL()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = path + "\\TeamworkPro";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        }

        #region Sync Service Logic
        public void PublishInOutLog()
        {
            try
            {
                log = new LogWriter("API PublishInOutLog ---> PublishInOutLog");
                List<TimingSync> objSyncList = new List<TimingSync>();
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spSyncTimeInTimeOut", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", GlobalVariable.EmployeeId);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            Project ps = new Project();
                            Tasks ts = new Tasks();
                            TimingSync objSync = new TimingSync();
                            objSync.Employee_Id = dr["Employee_Id"].ToString();
                            objSync.Employee_Name = dr["Employee_Name"].ToString();
                            objSync.Date = dr["Date"].ToString();
                            if(!string.IsNullOrEmpty(objSync.Date))
                            {
                                DateTime dtnew = Convert.ToDateTime(objSync.Date);
                                objSync.Date = dtnew.ToString("yyyy-dd-MM");
                            }
                            objSync.time_In = dr["time_In"].ToString();
                            var NewInTime = Convert.ToDateTime(objSync.time_In);
                            objSync.time_In= NewInTime.ToString("hh:mm:ss tt");

                            objSync.time_out = dr["time_out"].ToString();
                            var NewOutTime = Convert.ToDateTime(objSync.time_out);
                            objSync.time_out = NewOutTime.ToString("hh:mm:ss tt");

                            ps._id = dr["ProjectId"].ToString();
                            ps.pname= dr["pname"].ToString();
                            ts.summary = dr["TaskName"].ToString();
                            ts._id= dr["TaskId"].ToString();
                           ts.status = false;
                            objSync.Project = ps;
                            objSync.Task = ts;
                            objSyncList.Add(objSync);
                        }
                        if(objSyncList.Count> 0)
                        {
                            var json = new JavaScriptSerializer().Serialize(objSyncList);
                            var SyncUrl= TeamWorkPro.Properties.Settings.Default.SyncAPI;
                            log = new LogWriter($"API : {SyncUrl}");
                            var request = (HttpWebRequest)WebRequest.Create(SyncUrl);
                            request.Accept = "application/json";
                            request.Method = "POST";
                            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + GlobalVariable.Token);
                            log = new LogWriter($"API IN ---> {json}");
                            var data = Encoding.ASCII.GetBytes(json);
                            request.ContentType = "application/json";
                            request.ContentLength = data.Length;
                            using (var stream = request.GetRequestStream())
                            {
                                stream.Write(data, 0, data.Length);
                            }
                            var response = (HttpWebResponse)request.GetResponse();
                            var ResponseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                            dynamic stuff = JsonConvert.DeserializeObject(ResponseString);
                            log = new LogWriter($"API OUT ---> {ResponseString}");
                            int ResultOk = Convert.ToInt32(stuff.result.ok);
                            string address = stuff.result.n;
                            if(ResultOk >= 1)
                            {
                                UpdateSyncFlag();
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
            }
        }

        public void UpdateSyncFlag()
        {
            try
            {
                SqlCommand cmd = new SqlCommand("spUpdateFlag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
            }


        }

        public void SyncScreenShot()
        {
            try
            {
                log = new LogWriter("API PublishInOutLog ---> SyncScreenShot");
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spGetSavedScreenShotforSync", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", GlobalVariable.EmployeeId);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        Parallel.ForEach(dt.AsEnumerable(),  dr =>
                        {
                            SyncScreenShotModel objScreenShot = new SyncScreenShotModel();
                            var id = Convert.ToInt32(dr["Id"]);
                            objScreenShot.Name = dr["Name"].ToString();
                            var ImageUrl = dr["ImageUrl"].ToString();
                            objScreenShot.Date = dr["Date"].ToString();

                            if (!string.IsNullOrEmpty(objScreenShot.Date))
                            {
                                DateTime dtnew = Convert.ToDateTime(objScreenShot.Date);
                                objScreenShot.Date = dtnew.ToString("yyyy-dd-MM");
                                objScreenShot.Time = dtnew.ToString("hh:mm:ss tt");
                            }

                            objScreenShot.Employee_Id = GlobalVariable.EmployeeId;
                            using (Image image = Image.FromFile(ImageUrl))
                            {
                                using (MemoryStream m = new MemoryStream())
                                {
                                    image.Save(m, image.RawFormat);
                                    byte[] imageBytes = m.ToArray();
                                    objScreenShot.ImageBase64 = Convert.ToBase64String(imageBytes);
                                }
                            }
                            var json = new JavaScriptSerializer() { MaxJsonLength = 86753090 }.Serialize(objScreenShot);
                            var SyncUrl = TeamWorkPro.Properties.Settings.Default.SnapShopAPI;
                            log = new LogWriter($"API : {SyncUrl}");
                            var request = (HttpWebRequest)WebRequest.Create(SyncUrl);
                            request.Accept = "application/json";
                            request.Method = "POST";
                            log = new LogWriter($"API IN ---> {json}");
                            var data = Encoding.ASCII.GetBytes(json);
                            request.ContentType = "application/json";
                            request.ContentLength = data.Length;
                            using (var stream = request.GetRequestStream())
                            {
                                stream.Write(data, 0, data.Length);
                            }
                            var response = (HttpWebResponse)request.GetResponse();
                            var ResponseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                            dynamic stuff = JsonConvert.DeserializeObject(ResponseString);
                            log = new LogWriter($"API OUT ---> {ResponseString}");
                            int ResultOk = Convert.ToInt32(stuff.StatusCode);

                            if (ResultOk == 200)
                            {
                                UpdateScreenFlag(id);
                            }

                        });

                    }
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
            }
        }

        public void UpdateScreenFlag(int FlagId)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("spUpdateSyncFlag", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", GlobalVariable.EmployeeId);
                cmd.Parameters.AddWithValue("@FlagId", FlagId);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
            }


        }

        #endregion

    }
}
