﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using TeamWorkPro.DataLayer;

namespace TeamWorkPro.BusinessLayer
{
    public class TimingLogBaL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        LogWriter log = new LogWriter();
        public TimingLogBaL()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = path + "\\TeamworkPro";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        }
       
        #region TimeIn and TimeOut Logic

        public void SaveTimeInLog(TimeInTimeOutModel objLoginModel, string ProjectId, string Task, string TaskId)
        {
            try
            {
                DateTime dtnew = DateTime.Now;
                string Date = dtnew.ToString("yyyy/dd/M");
                string TimeIn = dtnew.ToString("HH:mm:ss");
                string TimeOut = dtnew.ToString("HH:mm:ss");
                SqlCommand cmd = new SqlCommand("spSaveTimeLogAndProjectDetailsLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", objLoginModel.Employee_id);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                cmd.Parameters.AddWithValue("@time_In", TimeIn);
                cmd.Parameters.AddWithValue("@time_out", TimeOut);
                cmd.Parameters.AddWithValue("@TimeType", objLoginModel.TimeType);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectId);
                cmd.Parameters.AddWithValue("@TaskId", TaskId);
                cmd.Parameters.AddWithValue("@TaskName", Task);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                con.Close();
            }
        }

        public void SaveTimeInLogForExistingTask(string Employee_id, string Task, int TaskID)
        {
            try
            {
                DateTime dtnew = DateTime.Now;
                string Date = dtnew.ToString("yyyy/dd/M");
                string TimeIn = dtnew.ToString("HH:mm:ss");
                string TimeOut = dtnew.ToString("HH:mm:ss");
                SqlCommand cmd = new SqlCommand("spSaveTimeLogAndProjectDetailsLogExstingTask", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_id);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                cmd.Parameters.AddWithValue("@time_In", TimeIn);
                cmd.Parameters.AddWithValue("@time_out", TimeOut);
                cmd.Parameters.AddWithValue("@TimeType", 1);
                cmd.Parameters.AddWithValue("@TaskName", Task);
                cmd.Parameters.AddWithValue("@TaskID", TaskID);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                con.Close();
            }
        }

        public void SaveTimeOutLog(TimeInTimeOutModel objLoginModel)
        {
            try
            {
                DateTime dtnew = DateTime.Now;
                string Date = dtnew.ToString("yyyy/dd/M");
                string TimeOut = dtnew.ToString("HH:mm:ss");
                int Id = GetMaxTiming(objLoginModel.Employee_id);
                SqlCommand cmd = new SqlCommand("spUpdateLogoutTiming", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", objLoginModel.Employee_id);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@time_out", TimeOut);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                con.Close();
            }
        }

        public void IdealSaveTimeOutLog(TimeInTimeOutModel objLoginModel)
        {
            try
            {
                DateTime dtnew = DateTime.Now.Subtract(new TimeSpan(0, 5, 0));
                string Date = dtnew.ToString("yyyy/dd/M");
                string TimeOut = dtnew.ToString("HH:mm:ss");
                int Id = GetMaxTiming(objLoginModel.Employee_id);
                SqlCommand cmd = new SqlCommand("spUpdateLogoutTiming", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", objLoginModel.Employee_id);
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@time_out", TimeOut);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                con.Close();
            }
        }

        public void SaveIdealTimeLog(TimeInTimeOutModel objLoginModel)
        {
            try
            {
                DateTime dtnew1 = DateTime.Now.Subtract(new TimeSpan(0, 5, 0));
                DateTime dtnew = DateTime.Now;
                string Date = dtnew.ToString("yyyy/dd/M");
                string Time_in = dtnew1.ToString("HH:mm:ss");
                string Time_out = dtnew.ToString("HH:mm:ss");
                int Id = GetMaxTiming(objLoginModel.Employee_id);
                SqlCommand cmd = new SqlCommand("spIdealTime", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", objLoginModel.Employee_id);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                cmd.Parameters.AddWithValue("@time_In", Time_in);
                cmd.Parameters.AddWithValue("@time_out", Time_out);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                con.Close();
            }
        }

        public int GetMaxTiming(string Employee_id)
        {
            int MaxTimingId = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spGetMaxTimingId", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_id);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            MaxTimingId = (int)(dr["Id"]);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                return MaxTimingId;
            }
            return MaxTimingId;
        }

        public string GetTotalWorkingHours(string Employee_id)
        {
            string TotalCount = "Time Logged Today: 00:00:00";
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spTimingCalculation", con);
                DateTime dtnew = DateTime.Now;
                string Date = dtnew.ToString();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_id);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            var Second = Convert.ToInt32(dr["Second"]);
                            var Hours = Convert.ToInt32(dr["Hours"]);
                            var Minute = Convert.ToInt32(dr["Minute"]);
                            if (Second >= 60)
                            {
                                int TempMinute = Second / 60;
                                Minute = (Minute + TempMinute);
                                Second = Second % 60;
                            }
                            if (Minute > 59)
                            {
                                int TempHours = Minute / 60;
                                Hours = (Hours + TempHours);
                                Minute = Minute % 60;
                            }
                            var DisplayHours = (Hours).ToString();
                            var DisplayMinute = (Minute).ToString();
                            var DisplaySecond = (Second).ToString();
                            if (Hours < 10)
                                DisplayHours = DisplayHours.PadLeft(2, '0');
                            if (Minute < 10)
                                DisplayMinute = DisplayMinute.PadLeft(2, '0');
                            if (Second < 10)
                                DisplaySecond = DisplaySecond.PadLeft(2, '0');
                            var TotalTimeCont = DisplayHours + ":" + DisplayMinute + ":" + DisplaySecond;

                            TotalCount = string.Empty;
                            TotalCount = "Time Logged Today: " + TotalTimeCont;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                return TotalCount;
            }
            return TotalCount;
        }

        public List<ProjectListModel> GetWorkingProjectList(string Employee_id)
        {
            List<ProjectListModel> objProjectList = new List<ProjectListModel>();
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spGetWorkingProject", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_id);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            ProjectListModel objProject = new ProjectListModel();
                            objProject.ProjectId = dr["_id"].ToString();
                            objProject.TaskId = dr["Id"].ToString();
                            objProject.TaskName = dr["TaskName"].ToString();
                            objProject.pname = dr["pname"].ToString();
                            objProjectList.Add(objProject);

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                return objProjectList;
            }
            return objProjectList;
        }

        public DataTable GetProjectList(string Employee_id)
        {
            DataTable dt = new DataTable();

            try
            {
                
                SqlCommand cmd = new SqlCommand("spProjectList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_id);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                   da.Fill(dt);
                    
                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                return dt;
            }
            return dt;
        }

        public DataTable GetTakList(string ProjectId)
        {
            DataTable dt = new DataTable();

            try
            {

                SqlCommand cmd = new SqlCommand("spGetTaskList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ProjectId", ProjectId);
                cmd.Parameters.AddWithValue("@Employee_Id", GlobalVariable.EmployeeId);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);

                }

            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
                return dt;
            }
            return dt;
        }

        public bool SaveProject(string ProjectId, string ProjectName)
        {
            log = new LogWriter("Save Project Start");
            bool IsSave = false;
            try
            {
                SqlCommand cmd = new SqlCommand("SaveProjectList", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId", GlobalVariable.EmployeeId);
                cmd.Parameters.AddWithValue("@ProjectId", ProjectId);
                cmd.Parameters.AddWithValue("@ProjectName", ProjectName);
                log = new LogWriter("go to open connection");
                log = new LogWriter(con.ConnectionString);

                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                    log = new LogWriter("connection open");
                }
                log = new LogWriter("start execute query");
                
                cmd.ExecuteNonQuery();
                log = new LogWriter("execute query successfully");
                con.Close();
                IsSave = true;
            }
            catch (Exception ex)
            {
                log = new LogWriter("Catch error");

                var errr = ex.Message;
                log = new LogWriter($"Error :-  {errr}");
                con.Close();
            }
            return IsSave;
        }

        public bool SaveTask(string EmployeeID, string ProjectID, string TaskID, string ProjectName)
        {
            bool IsSave = false;
            try
            {
                SqlCommand cmd = new SqlCommand("spSaveTask", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", EmployeeID);
                cmd.Parameters.AddWithValue("@Project_id", ProjectID);
                cmd.Parameters.AddWithValue("@Task_id", TaskID);
                cmd.Parameters.AddWithValue("@Taskname", ProjectName);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();
                con.Close();
                IsSave = true;
            }
            catch (Exception ex)
            {
                var errr = ex.Message;
                log = new LogWriter($"Error :-  {errr}");
                con.Close();
            }
            return IsSave;
        }

        public void GetProjectId(int TaskID)
        {
            try
            {
                DataTable dt = new DataTable();
                string Query = "select distinct ProjectId from ProjectTiminigLog where id=" + TaskID;
                SqlCommand cmd = new SqlCommand(Query, con);
                cmd.CommandType = CommandType.Text;
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            GlobalVariable.ProjectId = dr["ProjectId"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                var error = ex.Message;
                log = new LogWriter($"Error :-  {error}");
            }
        }
        public bool UpdateCompletedTask(int TaskID)
        {
            bool IsSave = false;
            try
            {
                GetProjectId(TaskID);
                SqlCommand cmd = new SqlCommand("spUpdateCompletedTask", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TaskID", TaskID);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();
                con.Close();
                IsSave = true;
            }
            catch (Exception ex)
            {
                var errr = ex.Message;
                log = new LogWriter($"Error :-  {errr}");
                con.Close();
            }
            return IsSave;
        }

        public bool DeleteProjectList()
        {
            bool IsSave = false;
            try
            {
                SqlCommand cmd = new SqlCommand("delete from ProjectDetails", con);
                cmd.CommandType = CommandType.Text;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();
                con.Close();
                IsSave = true;
            }
            catch (Exception ex)
            {
                var errr = ex.Message;
                log = new LogWriter($"Error :-  {errr}");
                con.Close();
            }
            return IsSave;
        }
        public bool DeleteTaskList()
        {
            bool IsSave = false;
            try
            {
                SqlCommand cmd = new SqlCommand("delete from TaskDetails", con);
                cmd.CommandType = CommandType.Text;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.ExecuteNonQuery();
                con.Close();
                IsSave = true;
            }
            catch (Exception ex)
            {
                var errr = ex.Message;
                log = new LogWriter($"Error :-  {errr}");
                con.Close();
            }
            return IsSave;
        }

        public void SaveScreenShot(string ImageName, string ImageUrl, string Employee_Id, string Base64Image)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("spSaveScreenShot", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Name", ImageName);
                cmd.Parameters.AddWithValue("@ImageUrl", ImageUrl);
                cmd.Parameters.AddWithValue("@Employee_Id", Employee_Id);
                cmd.Parameters.AddWithValue("@Base64Image", null);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int rowsAffected = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

                var erro = ex.Message;
                log = new LogWriter($"Error :-  {erro}");
            }


        }
        
        #endregion

    }
}
