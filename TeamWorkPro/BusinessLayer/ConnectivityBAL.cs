﻿using System;
using System.Net;

namespace TeamWorkPro.BusinessLayer
{
    public class ConnectivityBAL
    {
        public ConnectivityBAL()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = path + "\\TeamworkPro";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }
        public  bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
