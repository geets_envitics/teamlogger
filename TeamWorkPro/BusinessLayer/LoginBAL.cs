﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeamWorkPro.DataLayer;
namespace TeamWorkPro.BusinessLayer
{
    public class LoginBAL
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        public LoginBAL()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = path + "\\TeamworkPro";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        }

        #region AutoLogin If user checked Is remeber true then there is no need to login for next 30 Days
        public void SaveAutoLogin(LoginModel objLoginModel)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("spSaveAutoLogin", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Employee_Id", objLoginModel.Employee_id);
                cmd.Parameters.AddWithValue("@Employee_Name", objLoginModel.Employee_Name);
                cmd.Parameters.AddWithValue("@Token", objLoginModel.Token);
                cmd.Parameters.AddWithValue("@Date", DateTime.Now);
                cmd.Parameters.AddWithValue("@IsRemember", objLoginModel.IsRemembe);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                int i = cmd.ExecuteNonQuery();
                con.Close();


            }
            catch (Exception ex)
            {
                var error = ex.Message;
                con.Close();
            }
        }
        public bool GetAutoLogin()
        {
            bool IsLoggedIn = false;
            try
            {
                DataTable dt = new DataTable();
                string Query = "SELECT * FROM AutoLogin WHERE IsRemember=1 ";
                using (SqlCommand cmd = new SqlCommand(Query, con))
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                GlobalVariable.EmployeeId = dr["Employee_Id"].ToString();
                                GlobalVariable.EmployeeName = dr["Employee_Name"].ToString();
                                GlobalVariable.Token = dr["Token"].ToString();
                                var Date = dr["Date"].ToString();
                                DateTime TokenDate = Convert.ToDateTime(Date);
                                DateTime CurrentDate = DateTime.Now;
                                var TotalDays = (CurrentDate - TokenDate).TotalDays;
                                if (TotalDays <= 30)
                                    IsLoggedIn = true;
                                else
                                    IsLoggedIn = false;
                            }
                        }
                        else
                        {
                            IsLoggedIn = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                IsLoggedIn = false;
            }
            return IsLoggedIn;
        }
        public void DeleteAutoLogin()
        {
            try
            {
                DataTable dt = new DataTable();
                string DeleteQuery = "DELETE FROM AutoLogin";
                using (SqlCommand cmd = new SqlCommand(DeleteQuery, con))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    int rowsAffected = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                con.Close();
            }
        }
        #endregion


    }
}
