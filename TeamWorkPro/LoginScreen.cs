﻿using Newtonsoft.Json.Linq;
using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using TeamWorkPro.BusinessLayer;
using TeamWorkPro.DataLayer;

namespace TeamWorkPro
{
    public partial class LoginScreen : Form
    {
        private LoginBAL objLogin = new LoginBAL();
        private TimingLogBaL objLoginBAL = new TimingLogBaL();
        private ConnectivityBAL ObjectConnectivity = new ConnectivityBAL();
        private bool dragging = false;
        private Point startPoint = new Point(0, 0);
        private LogWriter log = new LogWriter();
        public LoginScreen()
        {
            InitializeComponent();

            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            if (!Directory.Exists(path +"\\TeamworkPro"))
            {
                Directory.CreateDirectory(path +"\\TeamworkPro");
            }
            path = path + "\\TeamworkPro";
            var filename = Application.StartupPath.ToString() + "\\TeamworkPro.mdf";
            var filenameldf = Application.StartupPath.ToString() + "\\TeamworkPro.mdf";
            var despath = path + "\\TeamworkPro.mdf";
            var desldfpath = path + "\\TeamworkPro.ldf";
            if (!File.Exists(despath))
            {
                File.Copy(filename, despath);
                File.Copy(filenameldf, desldfpath);
            }
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
        }

        private void LoginScreen_Load(object sender, EventArgs e)
        {
            bool IsLoggedIn = objLogin.GetAutoLogin();
            log = new LogWriter("Auto login done");
            if (IsLoggedIn)
            {
                GetProjectList();
                GetTaskList();
                HomeScreen myForm = new HomeScreen();
                this.Hide();
                myForm.ShowDialog();
                this.Close();
            }
        }

        private void Login_Click(object sender, EventArgs e)
        {
            bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
            try
            {
                if (!string.IsNullOrEmpty(txtUserName.Text) && !string.IsNullOrEmpty(txtPassword.Text))
                {
                    if (IsConnectivity)
                    {
                        object input = new
                        {
                            username = txtUserName.Text.Trim(),
                            password = txtPassword.Text.Trim(),
                        };
                        string inputJson = (new JavaScriptSerializer()).Serialize(input);
                        WebClient client = new WebClient();
                        client.Headers["Content-type"] = "application/json";
                        client.Encoding = Encoding.UTF8;
                        var APIUrl = TeamWorkPro.Properties.Settings.Default.LoginAPI;
                        var ResponseResult = client.UploadString(APIUrl, inputJson);
                        dynamic data = JObject.Parse(ResponseResult);
                        if (data.StatusCode == 400)
                        {

                            MessageBox.Show("Username or password is incorrect");
                        }
                        else if (data.StatusCode == 200)
                        {

                            DateTime dtnew = DateTime.Now;
                            string Date = dtnew.ToString("yyyy/dd/M");
                            GlobalVariable.CurrentDate = dtnew;
                            GlobalVariable.EmployeeId = data.Employee_Id;
                            GlobalVariable.EmployeeName = data.Employee_Name;
                            GlobalVariable.Token = data.token;
                            objLogin.DeleteAutoLogin();
                            bool IsRemember = ChkRememberMe.Checked;
                            LoginModel objLoginModel = new LoginModel();
                            objLoginModel.Employee_Name = GlobalVariable.EmployeeName;
                            objLoginModel.Employee_id = GlobalVariable.EmployeeId;
                            objLoginModel.Token = GlobalVariable.Token;
                            objLoginModel.IsRemembe = IsRemember;
                            objLogin.SaveAutoLogin(objLoginModel);
                            GetProjectList();
                            GetTaskList();
                            HomeScreen myForm = new HomeScreen();
                            this.Hide();
                            myForm.ShowDialog();
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show("please check your internet connection is working?");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Username or password is incorrect");
                var error = ex.Message;
                log = new LogWriter(error);
            }
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void btnMimimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnCancle_MouseHover(object sender, EventArgs e)
        {
            this.btnCancle.BackColor = Color.Red;
        }

        private void btnCancle_MouseLeave(object sender, EventArgs e)
        {
            this.btnCancle.BackColor = Color.Transparent;
        }

        private void PnlHeader_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            startPoint = new Point(e.X, e.Y);
        }

        private void PnlHeader_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.startPoint.X, p.Y - this.startPoint.Y);

            }
        }

        private void PnlHeader_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        public void GetProjectList()
        {
            log = new LogWriter("GetProjectList");
            //Get latest project for logged user if connectivity is availible
            bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
            try
            {
                if (IsConnectivity)
                {
                    WebClient client = new WebClient();
                    client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + GlobalVariable.Token);
                    client.Encoding = Encoding.UTF8;
                    string apiUrl = TeamWorkPro.Properties.Settings.Default.ProjectAPI;
                    var ResponseResult = client.DownloadString(apiUrl);
                    log = new LogWriter("API Response");
                    log = new LogWriter(ResponseResult);
                    JArray jsonArray = JArray.Parse(ResponseResult);
                    objLoginBAL.DeleteProjectList();
                    foreach (var obj in jsonArray.Children())
                    {
                        var itemProperties = obj.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "_id");
                        var myElementValue = myElement.Value.ToString();
                        var pnameElement = itemProperties.FirstOrDefault(x => x.Name == "pname");
                        var pnameElementValue = pnameElement.Value.ToString();
                        objLoginBAL.SaveProject(myElementValue, pnameElementValue);

                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter(error);
            }
        }

        public void GetTaskList()
        {
            //Get latest Tasklist for logged user if connectivity is availible
            bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
            try
            {
                if (IsConnectivity)
                {

                    WebClient client = new WebClient();
                    client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + GlobalVariable.Token);
                    client.Encoding = Encoding.UTF8;
                    string apiUrl = TeamWorkPro.Properties.Settings.Default.TaskListAPI;
                    var ResponseResult = client.DownloadString(apiUrl);
                    JArray jsonArray = JArray.Parse(ResponseResult);
                    objLoginBAL.DeleteTaskList();
                    foreach (var obj in jsonArray.Children())
                    {
                        var itemProperties = obj.Children<JProperty>();
                        var myElement = itemProperties.FirstOrDefault(x => x.Name == "_id");
                        var TaskID = myElement.Value.ToString();

                        var pnameElement = itemProperties.FirstOrDefault(x => x.Name == "summary");
                        var TaskName = pnameElement.Value.ToString();

                        var myValproject = itemProperties.FirstOrDefault(x => x.Name == "project");
                        var pnamemyValproject = myValproject.Value.ToString();
                        var ProjectID = string.Empty;
                        foreach (var obj1 in myValproject.Children())
                        {
                            var itemProperties1 = obj1.Children<JProperty>();
                            var myElement1 = itemProperties1.FirstOrDefault(x => x.Name == "_id");
                            ProjectID = myElement1.Value.ToString();
                        }



                        objLoginBAL.SaveTask(GlobalVariable.EmployeeId, ProjectID, TaskID, TaskName);

                    }
                }
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter(error);
            }
        }

        private void PnlHeader_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LoginScreen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
                try
                {
                    if (!string.IsNullOrEmpty(txtUserName.Text) && !string.IsNullOrEmpty(txtPassword.Text))
                    {
                        if (IsConnectivity)
                        {
                            object input = new
                            {
                                username = txtUserName.Text.Trim(),
                                password = txtPassword.Text.Trim(),
                            };
                            string inputJson = (new JavaScriptSerializer()).Serialize(input);
                            WebClient client = new WebClient();
                            client.Headers["Content-type"] = "application/json";
                            client.Encoding = Encoding.UTF8;
                            var APIUrl = TeamWorkPro.Properties.Settings.Default.LoginAPI;
                            var ResponseResult = client.UploadString(APIUrl, inputJson);
                            dynamic data = JObject.Parse(ResponseResult);
                            if (data.StatusCode == 400)
                            {

                                MessageBox.Show("Username or password is incorrect");
                            }
                            else if (data.StatusCode == 200)
                            {

                                DateTime dtnew = DateTime.Now;
                                string Date = dtnew.ToString("yyyy/dd/M");
                                GlobalVariable.CurrentDate = dtnew;
                                GlobalVariable.EmployeeId = data.Employee_Id;
                                GlobalVariable.EmployeeName = data.Employee_Name;
                                GlobalVariable.Token = data.token;
                                objLogin.DeleteAutoLogin();
                                bool IsRemember = ChkRememberMe.Checked;
                                LoginModel objLoginModel = new LoginModel();
                                objLoginModel.Employee_Name = GlobalVariable.EmployeeName;
                                objLoginModel.Employee_id = GlobalVariable.EmployeeId;
                                objLoginModel.Token = GlobalVariable.Token;
                                objLoginModel.IsRemembe = IsRemember;
                                objLogin.SaveAutoLogin(objLoginModel);
                                GetProjectList();
                                GetTaskList();
                                HomeScreen myForm = new HomeScreen();
                                this.Hide();
                                myForm.ShowDialog();
                                this.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("please check your internet connection is working?");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Username or password is incorrect");
                    var error = ex.Message;
                    log = new LogWriter(error);
                }
            }
        }
    }

}
