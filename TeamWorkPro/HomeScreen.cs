﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using TeamWorkPro.BusinessLayer;
using TeamWorkPro.DataLayer;


namespace TeamWorkPro
{
    public partial class HomeScreen : Form
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        LoginBAL objLogin = new LoginBAL();
        ServiceLogBAL objService = new ServiceLogBAL();
        TimingLogBaL objLoginBAL = new TimingLogBaL();
        ConnectivityBAL ObjectConnectivity = new ConnectivityBAL();
        private bool dragging = false;
        private Point startPoint = new Point(0, 0);
        LogWriter log = new LogWriter();
        public HomeScreen()
        {
            InitializeComponent();
            var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            path = path + "\\TeamworkPro";
            AppDomain.CurrentDomain.SetData("DataDirectory", path);
            TimerSync = new Timer();
            TimerSync.Tick += TimerSync_Tick;
            TimerSync.Interval = 5000;
            TimerSync.Start();
        }

        private void TimerSync_Tick(object sender, EventArgs e)
        {
            bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
            try
            {
                if (IsConnectivity)
                {
                    
                    TimerSync.Stop();
                    lblSynced.Text = "Syncing... please wait";
                    lblSynced.ForeColor = Color.Red;
                    objService.PublishInOutLog();
                    objService.SyncScreenShot();
                    lblSynced.Text = "Synced";
                    lblSynced.ForeColor = Color.Blue;
                    TimerSync.Start();
                    TimerSync.Interval = 60000;
                }
                else
                {
                    lblSynced.Text = "Working Offline";
                    lblSynced.ForeColor = Color.Red;
                }
            }
            catch (Exception ex)
            {

                var error = ex.Message;
                log = new LogWriter(error);
            }
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            objLogin.DeleteAutoLogin();
            TimerSync.Stop();
            LoginScreen myForm = new LoginScreen();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void btnStartTimer_Click(object sender, EventArgs e)
        {
            var ProjectID = string.Empty;
            var TaskId = string.Empty;
            try
            {
                ProjectID = cdProject.SelectedValue.ToString();// drpProject.SelectedValue;
                TaskId = cbTaskList.SelectedValue.ToString();

                if (!string.IsNullOrEmpty(TaskId) && !string.IsNullOrEmpty(ProjectID))
                {
                    GlobalVariable.ProjectId = ProjectID;
                    GlobalVariable.ProjectName = cdProject.Text;
                    GlobalVariable.TaskName = cbTaskList.Text;
                    TimeInTimeOutModel objTimeInOutModel = new TimeInTimeOutModel();
                    objTimeInOutModel.Employee_id = GlobalVariable.EmployeeId;
                    objTimeInOutModel.TimeType = 1;
                    objLoginBAL.SaveTimeInLog(objTimeInOutModel, ProjectID.ToString(), GlobalVariable.TaskName, TaskId.ToString());
                    TimerSync.Stop();
                    TimingScreen myForm = new TimingScreen();
                    this.Hide();
                    myForm.ShowDialog();
                    this.Close();
                }
                else
                {
                    if (string.IsNullOrEmpty(ProjectID))
                    {
                        MessageBox.Show("Please select project", "Teamwork");
                    }
                    else if (string.IsNullOrEmpty(TaskId))
                    {
                        MessageBox.Show("Please select task", "Teamwork");
                    }
                }
            }
            catch (Exception ex)
            {
                log = new LogWriter(ex.Message);
                if (string.IsNullOrEmpty(ProjectID))
                {
                    MessageBox.Show("Please select project", "Teamwork");
                }
                else if (string.IsNullOrEmpty(TaskId))
                {
                    MessageBox.Show("Please select task", "Teamwork");
                }
            }
        }

        private void btnCancle_Click(object sender, EventArgs e)
        {
            TimerSync.Stop();
            Environment.Exit(0);
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void HomeScreen_Load(object sender, EventArgs e)
        {
            bool IsConnectivity = ObjectConnectivity.CheckForInternetConnection();
            try
            {
                var path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                path = path + "\\TeamworkPro";
                AppDomain.CurrentDomain.SetData("DataDirectory", path);
                string Employee_id = GlobalVariable.EmployeeId;
                DataTable dt = objLoginBAL.GetProjectList(Employee_id);
                DataRow row = dt.NewRow();
                row[0] = "";
                row[1] = "";
                dt.Rows.InsertAt(row, 0);
                cdProject.DataSource = dt;
                //string.Concat(" - ", "pname", "_id") ;
                cdProject.DisplayMember = "pname";
                cdProject.ValueMember = "_id";
                if(!string.IsNullOrEmpty(GlobalVariable.ProjectId))
                    cdProject.SelectedValue = GlobalVariable.ProjectId;
                if(!string.IsNullOrEmpty(GlobalVariable.EmployeeName))
                {
                    lblUserName.Text = GlobalVariable.EmployeeName;
                }
                //Get saved project list if exists

                string totalWorkingHours = objLoginBAL.GetTotalWorkingHours(Employee_id);
                lblTotalTimeCount.Text = totalWorkingHours;
                
                //Syncing if connectivity

                CreateDynamicList(Employee_id);
                
            }
            catch (Exception ex)
            {
                var error = ex.Message;
                log = new LogWriter(error);
            }
        }

        private void btnCancle_MouseHover(object sender, EventArgs e)
        {
            this.btnCancle.BackColor = Color.Red;
        }

        private void btnCancle_MouseLeave(object sender, EventArgs e)
        {
            this.btnCancle.BackColor = Color.Transparent;
        }

        public void CreateDynamicList(string EmployeeId)
        {
            List<ProjectListModel> objProjectList = new List<ProjectListModel>();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HomeScreen));
            try
            {
                pnlProjectList.Controls.Clear();
                objProjectList = objLoginBAL.GetWorkingProjectList(EmployeeId);
                Label[] labels = new Label[objProjectList.Count];
                int yLocation = 20;
                int Count = 0;
                foreach (var itm in objProjectList)
                {
                    var TaskName = itm.TaskName;
                    var TaskId = itm.TaskId;
                    var ProjectID = itm.ProjectId;
                    var pname = itm.pname;
                    var ButtonName = TaskName + "$" + TaskId + "$" + pname;
                    var ContName = TaskId;
                    labels[Count] = new Label();

                    this.Controls.Add(labels[Count]);
                    labels[Count].Text = TaskName;
                    labels[Count].Font = new System.Drawing.Font("Microsoft Sans Serif", 8.85F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    labels[Count].AutoSize = true;
                    labels[Count].MaximumSize = new System.Drawing.Size(300, 0);

                    labels[Count].ForeColor = Color.Black;
                    labels[Count].Location = new Point(13, yLocation);

                    pnlProjectList.Controls.Add(labels[Count]);

                    Button btn = new Button();
                    btn.BackColor = System.Drawing.Color.Transparent;
                    btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    btn.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
                    btn.Location = new Point(340, yLocation);
                    btn.Name = ButtonName;
                    btn.Size = new System.Drawing.Size(22, 22);
                    btn.UseVisualStyleBackColor = false;
                    btn.TabStop = false;
                    btn.TabIndex = 5;
                    btn.FlatAppearance.BorderSize = 0;
                    btn.Click += Btn_Click;
                    pnlProjectList.Controls.Add(btn);

                    Button btnCnt = new Button();
                    btnCnt.BackColor = System.Drawing.Color.Transparent;
                    btnCnt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
                    btnCnt.Image = ((System.Drawing.Image)(resources.GetObject("testbtn.Image")));
                    btnCnt.Location = new Point(380, yLocation);
                    btnCnt.Name = ContName;
                    btnCnt.Size = new System.Drawing.Size(22, 22);
                    btnCnt.UseVisualStyleBackColor = false;
                    btnCnt.TabStop = false;
                    btnCnt.FlatAppearance.BorderSize = 0;
                    btn.TabIndex = 6;
                    btnCnt.Click += BtnCnt_Click;
                    pnlProjectList.Controls.Add(btnCnt);



                    yLocation = yLocation + 35;
                    Count = Count + 1;




                }

            }
            catch (Exception ex)
            {

                var message = ex.Message;
                log = new LogWriter(message);
            }
        }

        private void BtnCnt_Click(object sender, EventArgs e)
        {
            string s = (sender as Button).Name;
            if (!string.IsNullOrEmpty(s))
            {
                objLoginBAL.UpdateCompletedTask(Convert.ToInt32(s));
            }
            CreateDynamicList(GlobalVariable.EmployeeId);

        }

        private void Btn_Click(object sender, EventArgs e)
        {

            try
            {
                string s = (sender as Button).Name;
                string[] list = s.Split('$');

                var TaskId = 0;
                var TaskName = string.Empty;
                var ProjectName = string.Empty;
                if (list.Any())
                {
                    TaskName = list[0];
                    TaskId = Convert.ToInt32(list[1]);
                    ProjectName = list[2];
                }
                string Employee_id = GlobalVariable.EmployeeId;
                objLoginBAL.SaveTimeInLogForExistingTask(Employee_id, TaskName, TaskId);
                GlobalVariable.ProjectName = ProjectName;
                GlobalVariable.TaskName = TaskName;
                TimerSync.Stop();
                TimingScreen myTimingScreen = new TimingScreen();
                this.Hide();
                myTimingScreen.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {

                var error = ex.Message;
                log = new LogWriter(error);
            }



        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            startPoint = new Point(e.X, e.Y);
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.startPoint.X, p.Y - this.startPoint.Y);

            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void pnlProjectList_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cdProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cdProject.Text.Length > 1)
            {
                var ProjectId = cdProject.SelectedValue.ToString();
                DataTable dt = objLoginBAL.GetTakList(ProjectId.ToString());
                DataRow row = dt.NewRow();

                row[0] = "";
                row[1] = "";
                dt.Rows.InsertAt(row, 0);

                cbTaskList.DataSource = dt;
                cbTaskList.DisplayMember = "TaskName";
                cbTaskList.ValueMember = "TaskID";

                cbTaskList.AutoCompleteMode = AutoCompleteMode.Suggest;
                cbTaskList.AutoCompleteSource = AutoCompleteSource.ListItems;
            }
        }
    }
}
